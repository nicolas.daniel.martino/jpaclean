package com.demo.course;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.topic.Topic;

@RestController
public class CourseController {

	@Autowired // injects the dependency to the service
	private CourseService courseService;

	@RequestMapping("/topic/{id}/courses")
	public ResponseEntity<List<Course>> getAllCourse(@PathVariable("id") String topicId) {
		List<Course> course = courseService.getAllCoursesByTopicId(topicId);
		if (course.isEmpty())
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity(course, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value="/topic/{topicId}/course/{id}")
	public ResponseEntity<Course> getCourse(@PathVariable("id") String id) {
		Optional<Course> courseFetched = courseService.getCoursesById(id);
		if (!courseFetched.isPresent())
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<Course>(courseFetched.get(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/topic/{topicId}/course")
	public ResponseEntity<Course> addCourse(@PathVariable("topicId") String topicId, @RequestBody Course course) {
		course.setTopic(new Topic(topicId, "", ""));
		Optional<Course> courseAdded = courseService.addCourse(course);
		if (!courseAdded.isPresent())
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		else
			return new ResponseEntity<Course>(courseAdded.get(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/topic/{topicId}/course/{id}")
	public ResponseEntity<Course> updateCourse(@PathVariable("id") String id, @PathVariable("topicId") String topicId, @RequestBody Course course) {
		course.setTopic(new Topic(topicId, "", ""));
		Optional<Course> courseUpdate = courseService.updateCourse(course);
		if (!courseUpdate.isPresent())
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		else
			return new ResponseEntity<Course>(courseUpdate.get(), HttpStatus.OK);
	
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/topic/{topicId}/course/{id}")
	public ResponseEntity<Course> deleteCourse(@PathVariable("id") String id) {
		Optional<Course> courseDeleted = courseService.deleteCourseById(id);
		if (!courseDeleted.isPresent())
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		else
			return new ResponseEntity<Course>(courseDeleted.get(), HttpStatus.OK);
	

	}
}

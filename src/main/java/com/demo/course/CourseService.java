package com.demo.course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service // singleton
public class CourseService {
	@Autowired
	private CourseRepository courseRepository;

//	List<Course> topics = new ArrayList<>(Arrays.asList(new Course("1", "the one", "the one comes first"),
//			new Course("2", "the 2nd", "the 2nd comes after the first"),
//			new Course("3", "the 3rd", "the 3rd before the fourth"),
//			new Course("4", "the force", "may the force be with you")));

	// this allows for the list not to be create for each endpont call
	public List<Course> getAllCoursesByTopicId(String topicId) {
		List<Course> courses = new ArrayList<>();
		courseRepository.findAllCourseByTopicId(topicId).forEach(courses::add);
		return courses;
	}

	public Optional<Course> getCoursesById(String id) {
		return  courseRepository.findById(id);
	}

	public Optional<Course> addCourse(Course course) {
		courseRepository.save(course);
		return getCoursesById(course.getId());
	}

	public Optional<Course> updateCourse(Course course) {
		courseRepository.save(course);
		return getCoursesById(course.getId());
	}

	public Optional<Course> deleteCourseById(String id) {
		// TODO Auto-generated method stub
		Optional<Course> courseToDelete = getCoursesById(id);
		courseRepository.deleteById(id);
		return courseToDelete;
	}
}

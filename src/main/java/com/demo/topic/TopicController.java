package com.demo.topic;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicController {

	@Autowired // injects the dependency to the service
	private TopicService topicService;

	@RequestMapping("/topics")
	public ResponseEntity<List<Topic>> getTopics() {
		List<Topic> topics = topicService.getAllTopics();
		if (topics.isEmpty())
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity(topics, HttpStatus.OK);
	}

	@RequestMapping("/topic/{id}")
	public ResponseEntity<Topic> getTopic(@PathVariable("id") String id) {
		Optional<Topic> topicFetched = topicService.getTopicsById(id);
		if (!topicFetched.isPresent())
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<Topic>(topicFetched.get(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/topic")
	public ResponseEntity<Topic> addTopic(@RequestBody Topic topic) {
		Optional<Topic> topicAdded = topicService.addTopic(topic);
		if (!topicAdded.isPresent())
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		else
			return new ResponseEntity<Topic>(topicAdded.get(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/topic/{id}")
	public ResponseEntity<Topic> updateTopic(@PathVariable("id") String id, @RequestBody Topic topic) {
		Optional<Topic> topicUpdated = topicService.updateTopicById(topic);
		if (!topicUpdated.isPresent())
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		else
			return new ResponseEntity<Topic>(topicUpdated.get(), HttpStatus.OK);
	
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/topic/{id}")
	public ResponseEntity<Topic> deleteTopic(@PathVariable("id") String id) {
		Optional<Topic> topicDeleted = topicService.deleteTopicById(id);
		if (!topicDeleted.isPresent())
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		else
			return new ResponseEntity<Topic>(topicDeleted.get(), HttpStatus.OK);
	

	}
	@RequestMapping(method = RequestMethod.GET, value = "/topic/search/name/{name}")
	public ResponseEntity<List<Topic>> searchByName(@PathVariable("name") String name) {
		System.out.println(name);
		List<Topic> topicSearched = topicService.searchByName(name);
		if (topicSearched.isEmpty())
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		else
			return new ResponseEntity<List<Topic>>(topicSearched, HttpStatus.OK);
	

	}
}

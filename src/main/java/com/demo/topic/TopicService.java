package com.demo.topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service // singleton
public class TopicService {
	@Autowired
	private TopicRepository topicRepository;

	/*
	 * List<Topic> topics = new ArrayList<>(Arrays.asList(new Topic("1", "the one",
	 * "the one comes first"), new Topic("2", "the 2nd",
	 * "the 2nd comes after the first"), new Topic("3", "the 3rd",
	 * "the 3rd before the fourth"), new Topic("4", "the force",
	 * "may the force be with you")));
	 */

	// this allows for the list not to be create for each endpont call
	public List<Topic> getAllTopics() {
		List<Topic> topics = new ArrayList<>();
		topicRepository.findAll().forEach(topics::add);
		return topics;
	}

	public Optional<Topic> getTopicsById(String id) {
		return  topicRepository.findById(id);
	}

	public Optional<Topic> addTopic(Topic topic) {
		topicRepository.save(topic);
		return getTopicsById(topic.getId());
	}

	public Optional<Topic> updateTopicById(Topic topic) {
		topicRepository.save(topic);
		return getTopicsById(topic.getId());
	}

	public Optional<Topic> deleteTopicById(String id) {
		// TODO Auto-generated method stub
		Optional<Topic> topicToDelete = getTopicsById(id);
		topicRepository.deleteById(id);
		return topicToDelete;
	}
	public List<Topic> searchByName(String name) {

		List<Topic> topicSearched = topicRepository.findTopicByName(name);
		return topicSearched;
	}
}

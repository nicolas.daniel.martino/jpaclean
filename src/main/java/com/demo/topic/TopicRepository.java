package com.demo.topic;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TopicRepository extends CrudRepository<Topic, String> {

	List<Topic> findTopicByName(String name);
	
	List<Topic> findTopicByDescription(String description);

	
}
